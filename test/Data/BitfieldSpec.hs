{-# OPTIONS_GHC -Wno-type-defaults #-}

module Data.BitfieldSpec
  ( spec
  )
where

import           RIO
import qualified RIO.ByteString                as B
import           Data.Array
import           Data.Array.MArray
import           Control.Concurrent.STM         ( TArray )

import           Test.Hspec

import           Data.Piece
import           Data.Bitfield

tArrTest :: ByteString -> Array Word32 Bool -> IO ()
tArrTest bs arr = do
  let len = (\(x, y) -> y - x) $ bounds arr
  tArr :: TArray Word32 Bool <- atomically $ newArray (0, len) False
  -- test bytestring updates TArr
  updateBitfield True bs tArr
  newArr :: Array Word32 Bool <- atomically $ freeze tArr
  newArr `shouldBe` arr
  -- TArr goes to the bytestring
  let f x = if x then Got else Lack
      stateArr = fmap f newArr
  stateTArr <- atomically $ thaw stateArr
  newBS     <- tArrayToBitfield stateTArr
  newBS `shouldBe` bs

spec :: Spec
spec = describe "bitfield set" $ do
  it "01000101" $ do
    let bs = B.pack [2 ^ 6 + 2 ^ 2 + 1]
        arr :: Array Word32 Bool =
          listArray (0, 7) [False, True, False, False, False, True, False, True]
    tArrTest bs arr
  it "111100" $ do
    let bs  = B.pack ([2 ^ 7 + 2 ^ 6 + 2 ^ 5 + 2 ^ 4] :: [Word8])
        arr = listArray (0, 5) [True, True, True, True, False, False]
    tArrTest bs arr
  -- let's try some longer bits
  it "111100001" $ do
    let bs  = B.pack [2 ^ 7 + 2 ^ 6 + 2 ^ 5 + 2 ^ 4, 2 ^ 7]
        arr = listArray
          (0, 8)
          [True, True, True, True, False, False, False, False, True]
    tArrTest bs arr
  it "1010101010101010101010101010101010" $ do
    let bs = B.pack $ replicate 4 (2 ^ 7 + 2 ^ 5 + 2 ^ 3 + 2 ^ 1) <> [2 ^ 7]
        arr =
          listArray (0, 32) $ (concat . replicate 16 $ [True, False]) <> [True]
    tArrTest bs arr
