{-# OPTIONS_GHC -Wno-type-defaults #-}

module Data.PieceSpec
  ( spec
  )
where

import           RIO
import           RIO.FilePath
import qualified RIO.ByteString                as B
import           RIO.Directory

import           Test.Hspec

import           Data.MetaInfo
import           Data.Piece
import           System.Piece

checkFile :: ByteString -> FilePath -> IO Bool
checkFile bs fp = withFile fp ReadMode $ \h -> do
  contents <- B.hGetContents h
  return $ contents == bs

spec :: Spec
spec = describe "test write" $ do
  it "write multi" $ do
    Right mi <- liftIO
      $ readTorrent "./test/torrent-files/torrents/test-torrent-files.torrent"
    let rootfp = "./.tmp/downloads"
    createDirectory rootfp
    let file1 = "this is the first test file.\n"
        file2 = "This is the second\n\nbloop bloop.\n"
        file3 = "This is the 4th.\njk its the 3rd.\n"
        fs    = mi ^. infoDictL . filesL
    writeBlock (PieceInfo rootfp "doesn't matter" fs 0)
      $  file1
      <> file2
      <> file3
    f1 <- checkFile file1 $ rootfp </> "test1.txt"
    f1 `shouldBe` True
    f2 <- checkFile file2 $ rootfp </> "test2.txt"
    f2 `shouldBe` True
    f3 <- checkFile file3 $ rootfp </> "test3.txt"
    f3 `shouldBe` True
    -- read file and check it's still the same
    mvar <- newEmptyMVar
    readBlock (PieceInfo rootfp "doesn't matter" fs 0) 95 mvar
    bs <- takeMVar mvar
    bs `shouldBe` (file1 <> file2 <> file3)

  it "write single" $ do
    Right mi <- liftIO
      $ readTorrent "./test/torrent-files/torrents/randfile.torrent"
    let rootfp = "./.tmp/downloads"
    createDirectory rootfp
    let origfp = "./test/torrent-files/torrents/"
        fs     = mi ^. infoDictL . filesL
    mvar <- newEmptyMVar
    readBlock (PieceInfo origfp "randfile" fs (2 ^ 18)) (2 ^ 18) mvar
    bs <- takeMVar mvar
    writeBlock (PieceInfo rootfp "randfile" fs (2 ^ 18)) bs
    readBlock (PieceInfo rootfp "randfile" fs (2 ^ 18)) (2 ^ 18) mvar
    newbs <- takeMVar mvar
    -- liftIO $ print newbs
    -- True `shouldBe` False
    bs `shouldBe` newbs
