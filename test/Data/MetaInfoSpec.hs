module Data.MetaInfoSpec
  ( spec
  )
where

import           RIO

import           Test.Hspec

import           Data.MetaInfo

spec :: Spec
spec = describe "test read" $ do
  it "read valid" $ do
    x <- liftIO
      $ readTorrent "./test/torrent-files/torrents/test-torrent-files.torrent"
    isRight x `shouldBe` True

  it "read corrupt" $ do
    x <- liftIO $ readTorrent
      "./test/torrent-files/torrents/test-torrent-files-corrupt.torrent"
    isLeft x `shouldBe` True
