{-# OPTIONS_GHC -Wno-type-defaults #-}

module Protocol.TrackerSpec where

import           RIO
import           Test.Hspec

import           Network.Socket                 ( SockAddr(SockAddrInet)
                                                , tupleToHostAddress
                                                )
import           Protocol.Tracker

spec :: Spec
spec = describe "Binary Peers" $ do
  it "127.0.0.1" $ do
    let bp = binaryPeers "\127\0\0\1\68\81"
    bp `shouldBe` Just
      [SockAddrInet (68 * 16 ^ 2 + 81) (tupleToHostAddress (127, 0, 0, 1))]

  it "192.168.0.1:6881" $ do
    let bp = binaryPeers "\192\168\0\1\68\81"
    bp `shouldBe` Just
      [SockAddrInet (68 * 16 ^ 2 + 81) (tupleToHostAddress (192, 168, 0, 1))]
