module Main where

import           RIO
import           RIO.Process

import           Options.Applicative

import           AppOptions
import           Data.App
import           Data.Config
import           Data.Logging
import           Lib

main :: IO ()
main = do
  pc      <- mkDefaultProcessContext
  options <- execParser opt
  config  <- getConfig options >>= \case
    Just c  -> pure c
    Nothing -> error "Couldn't generate config"

  withHLog defaultOpts $ \lf ->
    let app = App { _logFunc    = lf
                  , _processCtx = pc
                  , _config     = config
                  , _logOpts    = defaultOpts
                  }
    in  runRIO app htd
