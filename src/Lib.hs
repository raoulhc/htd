module Lib
  ( htd
  )
where

import           RIO                     hiding ( logDebug
                                                , logInfo
                                                , logWarn
                                                , logError
                                                )

import           Data.App
import           Data.Logging

htd :: RIO App ()
htd = do
  logDebug "debug"
  logInfo "info"
  logWarn "warn"
  logError "error"
