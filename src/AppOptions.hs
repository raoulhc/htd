{-|
Module      : AppOptions
Copyright   : (c) Raoul Hidalgo Charman, 2019
License     : BSD
Maintainer  : raoul.hidalgo.charman@gmail.com
Stability   : experimental

Data structure for options passed to the app, and a command line parser for them
-}
module AppOptions where

import           RIO
import           Options.Applicative
import           Utils

data AppOptions = AppOptions
  { _verbose :: !(Maybe Bool)
  , _sessionDir :: !(Maybe FilePath)
  , _aConfig :: !(Maybe FilePath)
  , _file :: !FilePath
  }

makeLenses ''AppOptions

optParser :: Parser AppOptions
optParser =
  AppOptions
    <$> optional
          (flag False True $ long "verbose" <> short 'v' <> help "debug logging"
          )
    <*> optional
          (  strOption
          $  long "session"
          <> short 's'
          <> help "session directory"
          <> metavar "FILE"
          )
    <*> optional
          (strOption $ long "config" <> short 'c' <> help
            "Specify a configuration file"
          )
    <*> argument str (metavar "FILE" <> help "Torrent file to start")

opt :: ParserInfo AppOptions
opt = info (optParser <**> helper)
           (fullDesc <> progDesc "htd - Haskell Torrent Daemon")

