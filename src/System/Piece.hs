{-|
Module      : System.Piece
Copyright   : (c) Raoul Hidalgo Charman, 2019
License     : BSD
Maintainer  : raoul.hidalgo.charman@gmail.com

Methods for writing data to files
-}
module System.Piece
  ( writeBlockM
  , readBlockM
  , startFileThread
  -- These shouldn't be needed, might be later worth moving into an internal file
  , readBlock
  , writeBlock
  )
where

import           RIO                     hiding ( logDebug
                                                , logWarn
                                                )
import           RIO.List
import           RIO.Partial                    ( toEnum )
import           RIO.FilePath
import qualified Data.ByteString.Char8         as B
import           GHC.IO.Handle

import           Data.Torrent
import           Data.MetaInfo
import           Data.Piece
import           Data.Logging
import           Utils

-- TODO I may want to consider later whether we should be opening files for each
-- piece or keeping them open

---------
-- API --
---------

-- | Write a chunk give by a piece location and bytestring to it's respective file
writeBlockM
  :: (HasHLog x, MonadReader x m, HasTorrentEnv x, MonadIO m)
  => PieceLoc
  -> ByteString
  -> m ()
writeBlockM loc payload = do
  logDebug "Writing block to queue"
  blockInfo <- getPieceInfo loc
  env       <- getEnv
  let q = env ^. writeFileQL
  atomically $ writeTQueue q (blockInfo, payload)

-- | Read a chunk given a bytestring and it's length
readBlockM
  :: (HasHLog x, MonadReader x m, HasTorrentEnv x, MonadIO m)
  => PieceLoc
  -> Word32
  -> m ByteString
readBlockM loc len = do
  logDebug "Reading block from queue"
  pieceInfo <- getPieceInfo loc
  env       <- getEnv
  let q = env ^. readFileQL
  -- TODO might want to keep an MVar around rather than a new one every time but
  -- I'm not sure of the best approach
  mvar <- newEmptyMVar
  atomically . writeTQueue q $ (pieceInfo, len, mvar)
  readMVar mvar

getPieceInfo
  :: (MonadReader x m, HasTorrentEnv x, MonadIO m) => PieceLoc -> m PieceInfo
getPieceInfo (PieceLoc ix offset) = do
  env <- getEnv
  let fs       = env ^. metaInfoL . infoDictL . filesL
      pieceLen = env ^. metaInfoL . infoDictL . pieceLengthL
      name     = env ^. metaInfoL . infoDictL . nameL
      tvar     = env ^. downloadPathL
      loc      = ix * pieceLen + offset
  rootfp <- readTVarIO tvar
  return $ PieceInfo rootfp name fs loc

---------------------------
-- File thread functions --
---------------------------

-- | Data structure for file thread environment
data FileEnv = FileEnv
  { _fLogFunc :: !HLog
  , _fReadFileQ :: !ReadQueue
  , _fWriteFileQ :: !WriteQueue
  }

makeLenses ''FileEnv

instance HasHLog FileEnv where
  hLogL = fLogFuncL

newtype FileM a = FileM { runFileM :: RIO FileEnv a}
  deriving (Functor, Applicative, Monad, MonadIO, MonadUnliftIO, MonadReader FileEnv)

-- | Create necessary environment for the file thread and start it returning async
-- and read and write queues
startFileThread
  :: MonadUnliftIO m
  => MonadIO m => HLogOpts -> m (Async (), ReadQueue, WriteQueue)
startFileThread lo = do
  rQ <- newTQueueIO
  wQ <- newTQueueIO
  withHLog lo $ \lf -> do
    let env = FileEnv lf rQ wQ
    a <- async . runRIO env $ runFileM fileIO
    return (a, rQ, wQ)

-- | Logic for a thread reading and writing from file queues
fileIO :: FileM ()
fileIO = do
  logDebug "Starting file IO thread"
  forever $ do
    rQ     <- view fReadFileQL
    wQ     <- view fWriteFileQL
    (r, w) <- atomically $ do
      r <- tryReadTQueue rQ
      w <- tryReadTQueue wQ
      case (r, w) of
        (Nothing, Nothing) -> retrySTM
        x                  -> pure x
    mapM_ (uncurry writeBlock)            w
    mapM_ (\(x, y, z) -> readBlock x y z) r

-------------
-- Plumbing --
-------------

-- | Write a block given file info and payload
writeBlock :: MonadIO m => PieceInfo -> ByteString -> m ()
writeBlock (PieceInfo rootfp name fs loc) payload = either
  (const $ writeBlockSingle (rootfp </> B.unpack name) loc payload)
  (\x -> writeBlockMulti rootfp loc x payload)
  fs

-- | Read a block given a length and put it back in the MVar
readBlock :: MonadIO m => PieceInfo -> Word32 -> MVar ByteString -> m ()
readBlock (PieceInfo rootfp name fs loc) len mvar = do
  bs <- either (const $ readBlockSingle (rootfp </> B.unpack name) loc len)
               (readBlockMulti rootfp loc len)
               fs
  putMVar mvar bs

-- | Write a piece to a single file, nothing fancy, just need to set handle
-- position to location
writeBlockSingle :: MonadIO m => FilePath -> Word32 -> ByteString -> m ()
writeBlockSingle fp loc payload = liftIO $ withFile fp ReadWriteMode $ \h -> do
  hSetPosn (HandlePosn h (toInteger loc))
  B.hPut h payload

-- writes multiple chunks
-- first finds index and offest for the first file
-- then splits up the payload with appropriate file
-- finally writes these bits to the appropriate files
writeBlockMulti
  :: MonadIO m => FilePath -> Word32 -> [FileInfo] -> ByteString -> m ()
writeBlockMulti rootfp loc fs payload = do
  let (firstix, offset) = filePositions loc fs
      filelens = drop (fromEnum firstix) . fmap (fromEnum . _fileLength) $ fs
      filespayload = zip fs . splitByteString payload $ filelens
  case filespayload of
    []             -> return ()
    ((x, y) : xys) -> do
      writeFile offset x y
      mapM_ (uncurry $ writeFile (0 :: Int)) xys
 where
  writeFile offset file pl = do
    let filepath = B.unpack . B.intercalate "/" $ _path file
    liftIO $ withFile
      (rootfp </> filepath)
      WriteMode
      (\h -> do
        hSetPosn $ HandlePosn h (toInteger offset)
        B.hPut h pl
      )

-- | Read block from a single file
readBlockSingle :: MonadIO m => FilePath -> Word32 -> Word32 -> m ByteString
readBlockSingle fp loc len = liftIO . withFile fp ReadMode $ \h -> do
  hSetPosn (HandlePosn h (toInteger loc))
  B.hGet h $ fromEnum len

-- | Read block potentially across multiple files
readBlockMulti
  :: MonadIO m => FilePath -> Word32 -> Word32 -> [FileInfo] -> m ByteString
readBlockMulti rootfp loc len fs = do
  let (firstix, offset) = filePositions loc fs
      files             = drop (fromEnum firstix) fs
      allfiles =
        zip (offset : repeat 0)
          . fmap fst
          . takeWhile (\x -> snd x <= len)
          . zip files
          $ scanl' (\x y -> _fileLength y + x) 0 files
  fst
    <$> foldM
          (\(bytestring, remaining) (off, file) -> do
            (bs, remaining') <- readFile off remaining file
            return (bytestring <> bs, remaining')
          )
          ("", len)
          allfiles
 where
  readFile offset remaining file = do
    let filepath = foldl' (</>) rootfp . fmap B.unpack $ _path file
    bs <- readBlockSingle filepath offset remaining
    return (bs, remaining - toEnum (B.length bs))

-- | Get first index and offset from list of files
filePositions :: Word32 -> [FileInfo] -> (Word32, Word32)
filePositions location = foldl' accum (0, location)
 where
  accum (ix, loc) file | loc < fileLen = (ix, loc)
                       | otherwise     = (ix + 1, loc - fileLen)
    where fileLen = _fileLength file

-- | Given a list of lengths, split a bytestring into multiple, aux function for
-- writing to multiple files
splitByteString :: ByteString -> [Int] -> [ByteString]
splitByteString "" _  = []
splitByteString _  [] = []
splitByteString bs (sp : splits) =
  let (x, xs) = B.splitAt sp bs in x : splitByteString xs splits
