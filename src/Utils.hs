module Utils where

import           RIO
import           RIO.List.Partial
import           Control.Lens                   ( (&)
                                                , (.~)
                                                )
import           Control.Lens.TH
import           Language.Haskell.TH

-- Name lenses with L appended to differentiate
makeLenses :: Name -> DecsQ
makeLenses = makeLensesWith $ lensRules & lensField .~ \_ _ name ->
  [TopName (mkName . tail $ nameBase name ++ "L")]

-- | Expand ~/ to provided home directory
expandHome :: FilePath -> FilePath -> FilePath
expandHome home ('~' : fp) = home <> fp
expandHome _    fp         = fp
