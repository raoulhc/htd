{-|
Module      : Data.Peer
Copyright   : (c) Raoul Hidalgo Charman, 2019
License     : BSD
Maintainer  : raoul.hidalgo.charman@gmail.com
Stability   : experimental

This contains data structures related to peers.
-}

module Data.Peer where

import           RIO

import           Network.Socket
import           Control.Concurrent.STM.TArray
import           Control.Concurrent.STM.TSem

import           Data.Piece
import           Data.Logging
import           Data.Torrent
import           Utils

-- | Exceptions thrown by peers
data PeerException
  = PieceOutOfBounds
  | UnexpectedResponse
  | UnrequestedPiece Word32
  | CorruptedResponse
  | EmptyResponse
  | FailedHandshake
  | PeerTimeout
  | PeerFinished
  | Unimplemented
  deriving Show

instance Exception PeerException

-- | Default time to drop a connection after no message has been received
defaultTimeout :: Word32
defaultTimeout = 300

-- | Peer environment, containing immutable and transaction data that is shared
--   among the different threads
data PeerEnv = PeerEnv
  { _torrentR :: TorrentEnv
  , _pid :: !(TVar (Maybe ByteString))
  , _socket :: !Socket
  , _amChoke :: !(TVar Bool)
  , _amInterested :: !(TVar Bool)
  , _pChoke :: !(TVar Bool)
  , _pInterested :: !(TVar Bool)
  , _timeout :: !(TVar Word32)
  , _pPieceList :: !(TArray Word32 Bool)
  , _outReqs :: !TSem
  , _inReqs :: !(TBQueue (PieceLoc, Word32))
  , _pFinishSig :: !(MVar ())
  , _pHLog :: !HLog
  }

makeLenses ''PeerEnv

instance HasTorrentEnv PeerEnv where
  envL = lens _torrentR (\x y -> x { _torrentR = y })

instance HasHLog PeerEnv where
  hLogL = pHLogL

-- | RIO monad containing peer environment
newtype PeerMonad a = PeerMonad { runPeerMonad :: RIO PeerEnv a }
  deriving ( Functor, Applicative, Monad, MonadIO , MonadReader PeerEnv
           , MonadUnliftIO)
