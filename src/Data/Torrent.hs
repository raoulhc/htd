{-# OPTIONS_GHC -Wno-orphans #-}
{-|
Module      : Data.Torrent
Copyright   : (c) Raoul Hidalgo Charman, 2019
License     : BSD
Maintainer  : raoul.hidalgo.charman@gmail.com
Stability   : experimental

Contains data structures for torrent environment and tracker response as well as
relevant instances.
-}

module Data.Torrent where

import qualified RIO.HashMap                   as H
import           RIO
import           Data.Hashable                  ( Hashable(..) )
import           Control.Concurrent.STM.TArray
import           Network.Socket                 ( SockAddr
                                                , Socket
                                                )

import           Data.Config
import           Data.Piece                     ( PieceState
                                                , PieceInfo
                                                )
import           Data.Logging
import           Data.MetaInfo

import           Utils                          ( makeLenses )

-- | As we use hash maps with addr keys, might be a more effecient way of doing
-- this
instance Hashable SockAddr where
  hashWithSalt salt addr = hashWithSalt salt $ show addr

-- | Data struction for what a tracker will respond with
data TrackerResponse = TrackerResponse
    { _interval :: !Word32
    , _peerAddrs :: ![SockAddr]
    , _minInterval :: !(Maybe Word32)
    , _seeders :: !(Maybe Word32)
    , _leechers :: !(Maybe Word32)
    } deriving (Eq, Show)

makeLenses ''TrackerResponse

maxPeers :: Int
maxPeers = 20

-- | Data stored by a torrent's connection with each peer
data PeerInfo = PeerInfo
  { _piID :: !(Maybe ByteString)
  , _piSocket :: !(Maybe Socket)
  , _piInUse :: !Bool
  , _piDropped :: !Bool
  , _piSignal :: !(MVar ())
  }

makeLenses ''PeerInfo

-- | Data structure for immutable and shared data for a single torrent
data TorrentEnv = TorrentEnv
  { _tConfig :: !Config
  , _metaInfo :: !MetaInfo
  , _peerID :: !ByteString
  , _trackerInfo :: !(TVar (Maybe TrackerResponse))
  , _downloadPath :: !(TVar FilePath)
  , _pieceBitField :: !(TArray Word32 PieceState)
  , _piecesLeft :: !(TVar Word32)
  , _finished :: !(TVar Bool)
  , _peers :: !(TVar (H.HashMap SockAddr PeerInfo))
  , _connectedPeers :: !(TVar Int)
  , _torrLogFunc :: !HLog
  , _logOptions :: !HLogOpts
  , _readFileQ :: !ReadQueue
  , _writeFileQ :: !WriteQueue
  }

type PeerMap = H.HashMap SockAddr PeerInfo

type ReadQueue = TQueue (PieceInfo, Word32, MVar ByteString)
type WriteQueue = TQueue (PieceInfo, ByteString)

makeLenses ''TorrentEnv

-- | Class for accessing torrent enviroment in different monads
class HasTorrentEnv env where
  envL :: Lens' env TorrentEnv
  getEnv :: MonadReader env m => m TorrentEnv
  getEnv = view envL

instance HasTorrentEnv TorrentEnv where
  envL = id

instance HasHLog TorrentEnv where
  hLogL = torrLogFuncL

-- | RIO monad that contains the torrent enviroment
newtype TorrentStack a = TorrentStack { runTorrentStack :: RIO TorrentEnv a }
    deriving (Functor, Applicative, Monad, MonadIO, MonadUnliftIO,
              MonadReader TorrentEnv)
