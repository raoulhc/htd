{-|
Module      : Data.Message
Copyright   : (c) Raoul Hidalgo Charman, 2019
License     : BSD
Maintainer  : raoul.hidalgo.charman@gmail.com
Stability   : experimental

This contains the Message data type and serialize instance for it.
-}

module Data.Message
  ( Message(..)
  )
where

import           RIO
import qualified RIO.ByteString                as B
import           RIO.Partial
import           ByteString.StrictBuilder
import           Data.Serialize
import           Control.Monad                  ( replicateM )

import           Data.Piece

-- | All bittorrent message types
data Message
  = KeepAlive
  | Choke
  | Unchoke
  | Interested
  | Uninterested
  | Have Word32
  | Bitfield ByteString
  | Request PieceLoc Word32
  | Piece PieceLoc ByteString
  | Cancel PieceLoc Word32
  | Port Word64
  deriving (Eq, Show)

-- | Methods for serialising and deserialising messages to bytestrings
instance Serialize Message where
  put = serializeMessage
  get = deserializeMessage

-- | Put monad that takes a message
serializeMessage :: Message -> Put
serializeMessage msg = do
  let bs = case msg of
        KeepAlive                -> ""
        Choke                    -> "\0"
        Unchoke                  -> "\1"
        Interested               -> "\2"
        Uninterested             -> "\3"
        Have     x               -> "\4" <> builderBytes (word32BE x)
        Bitfield x               -> "\5" <> x
        Request (PieceLoc x y) z -> "\6" <> w32ToBytes [x, y, z]
        Piece   (PieceLoc x y) z -> "\7" <> w32ToBytes [x, y] <> z
        Cancel  (PieceLoc x y) z -> "\8" <> w32ToBytes [x, y, z]
        Port x                   -> "\9" <> builderBytes (word64BE x)
      len = toEnum . B.length $ bs :: Word32
  putWord32be len
  putByteString bs
  where w32ToBytes xs = let f = (builderBytes . word32BE) in foldMap f xs

-- | Get monad for message
deserializeMessage :: Get Message
deserializeMessage = do
  xs <- fromEnum <$> getWord32be
  r  <- remaining
  when (xs /= r)
    .  fail
    $  "Length prefix didn't match payload: "
    <> "Actual ("
    <> show r
    <> ") vs Received ("
    <> show xs
    <> ")"
  if r == 0
    then return KeepAlive
    else do
      mid <- getInt8
      case mid of
        0 -> return Choke
        1 -> return Unchoke
        2 -> return Interested
        3 -> return Uninterested
        4 -> Have <$> getWord32be
        5 -> do
          bitfield <- getByteString (r - 1)
          return . Bitfield $ bitfield
        6 -> do
          [x, y, z] <- replicateM 3 getWord32be
          return $ Request (PieceLoc x y) z
        7 -> do
          [x, y] <- replicateM 2 getWord32be
          z      <- remaining >>= getByteString
          return $ Piece (PieceLoc x y) z
        8 -> do
          [x, y, z] <- replicateM 3 getWord32be
          return $ Cancel (PieceLoc x y) z
        9 -> Port <$> getWord64be
        _ -> fail "Undefined ID"
