{-|
Module      : Data.Progress
Copyright   : (c) Raoul Hidalgo Charman, 2019
License     : BSD
Maintainer  : raoul.hidalgo.charman@gmail.com
Stability   : experimental

Data structures relating to how progress of a torrent is saved.
-}

module Data.Progress where

import           RIO                     hiding ( logDebug
                                                , logInfo
                                                , logWarn
                                                , logError
                                                )
import qualified RIO.ByteString                as B
import           RIO.Directory
import           RIO.FilePath
import qualified RIO.Map                       as M

import           Control.Exception              ( throw )
import           Control.Lens                   ( (^?) )
import qualified Data.Serialize                as S
import           Data.Bits                      ( popCount )
import           Text.Printf

import           Data.Bitfield
import           Data.Config
import           Data.Logging
import           Data.Bencoding
import           Data.Torrent
import           Data.MetaInfo
import           Data.Piece

data ProgressException
  = CorruptedProgress
  deriving Show

instance Exception ProgressException

saveProgress :: (MonadReader TorrentEnv m, MonadIO m) => m ()
saveProgress = do
  logInfo "Saving progress"
  env <- ask
  ben <- envToBen env
  let sessionDir = env ^. tConfigL . cfgSessionPathL
      name =
        concatMap (printf "%02x") . B.unpack $ env ^. metaInfoL . infoHashL
  writeFileBinary (sessionDir </> name <> ".htd_prog") $ S.encode ben

loadProgress :: (MonadReader TorrentEnv m, MonadIO m) => m ()
loadProgress = do
  logInfo "Loading progress"
  sessionDir <- view $ tConfigL . cfgSessionPathL
  infoHash   <- view $ metaInfoL . infoHashL
  let name        = concatMap (printf "%02x") . B.unpack $ infoHash
      sessionFile = sessionDir </> name <> ".htd_prog"
  load <- doesFileExist sessionFile
  when load $ do
    x <- S.decode @Bencoding <$> readFileBinary sessionFile
    case x of
      Left  _   -> throw CorruptedProgress
      Right ben -> benToEnv ben
    count <- view piecesLeftL >>= readTVarIO
    logInfo $ "Loaded progress, " <> displayShow count <> " pieces left"

-- | Given a torrent environment creates a bencoding
envToBen :: MonadIO m => TorrentEnv -> m Bencoding
envToBen env = do
  let tArray = _pieceBitField env
  bs <- tArrayToBitfield tArray
  return . BDict $ M.fromList [("bitfield", BStr bs)]

-- | Bencoding to a torrent environment
benToEnv :: (MonadReader TorrentEnv m, MonadIO m) => Bencoding -> m ()
benToEnv ben = do
  let mbs = ben ^? key "bitfield" . _BStr
  case mbs of
    Nothing -> throw CorruptedProgress
    Just bs -> do
      bitfield <- view pieceBitFieldL
      updateBitfield Got bs bitfield
      count <- view piecesLeftL
      size  <- view $ metaInfoL . pieceNumberL
      let pieces =
            size - fromIntegral (B.foldl' (\acc w8 -> popCount w8 + acc) 0 bs)
      atomically $ writeTVar count pieces
