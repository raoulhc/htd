{-|
Module      : Data.Bitfield
Copyright   : (c) Raoul Hidalgo Charman, 2019
License     : BSD
Maintainer  : raoul.hidalgo.charman@gmail.com
Stability   : experimental

This contains helper methods for updating bitfield via bytestrings that
represent them.
-}

module Data.Bitfield
  ( BitfieldException
  , updateBitfield
  , tArrayToBitfield
  )
where

import           RIO
import           RIO.Partial
import qualified RIO.ByteString                as BS
import           Control.Exception              ( throw )
import           Control.Concurrent.STM.TArray
import           Data.Array.MArray
import qualified Data.Bits                     as B

import           Data.Piece

-- | Exception types for bitfields methods
data BitfieldException
  = BitfieldLengthMismatch
  | EmptyByteString
  deriving (Show)

instance Exception BitfieldException

-- | Update TArray using Array. Used for incoming peer bitfields to find out
--   which pieces we want, updating the current bitfield with the pieces it has
--   marked as available
updateBitfield :: MonadIO m => b -> ByteString -> TArray Word32 b -> m ()
updateBitfield trueish bs tArr = do
  let bsLen = toEnum $ BS.length bs :: Word32
  size <- (\(x, y) -> y - x + 1) <$> atomically (getBounds tArr)
  -- throw error if bytestring is wrong size
  when
      (if size `mod` 8 == 0
        then size `div` 8 /= bsLen
        else size `div` 8 /= bsLen - 1
      )
    $ throw BitfieldLengthMismatch
  -- we need to treat the last byte differently
  (rest, last) <- case BS.unsnoc bs of
    Just x  -> return x
    Nothing -> throw EmptyByteString
  -- fold over all of the bytestring
  n <- foldM f 0 (BS.unpack rest)
  foldM_ (g last n) () [0 .. 7]
 where
    -- fold over byte
  f pos w8 = do
    foldM_ (g w8 pos) () [0 .. 7]
    return (pos + 8)
  -- test bits
  g w8 gPos _ pos = when (B.testBit w8 (fromEnum $ 7 - pos))
                         (atomically $ writeArray tArr (gPos + pos) trueish)

-- | Generate bytestring from current master piece status. Used when we want to
--   send a bitfield to a new connection.
tArrayToBitfield :: MonadIO m => TArray Word32 PieceState -> m ByteString
tArrayToBitfield tArr = do
  size <- (\(x, y) -> y - x + 1) <$> atomically (getBounds tArr)
  let quotient = size `div` 8
      bs :: [(Word32, Word32)] =
        zip [0, 8 ..] (replicate (fromEnum quotient) 7)
          <> if size `mod` 8 == 0
             then
               []
             else
               [(quotient * 8, size `mod` 8 - 1)]
  -- set bits on word8 and then pack
  BS.pack <$> mapM f bs
 where
    -- set byte
  f :: MonadIO m => (Word32, Word32) -> m Word8
  f (gPos, pos) = foldM (g gPos) (0 :: Word8) [0 .. pos]
  -- set bit
  g gPos w8 pos = do
    state <- atomically $ readArray tArr (gPos + pos)
    return $ case state of
      Got -> B.setBit w8 (fromEnum (7 - pos))
      _   -> w8
