{-|
Module      : Data.UrlEncode
Description : URL encode methods
Copyright   : (c) Raoul Hidalgo Charman, 2019
License     : BSD
Maintainer  : raoul.hidalgo.charman@gmail.com
Stability   : experimental

Methods for encoding a string in a url valid fashion. `urlEncodeVars` does this
for a set of url vars used in http requests.
-}

module Data.UrlEncode where

import           RIO
import           RIO.Partial
import qualified Data.ByteString.Char8         as B
import           Data.Char                      ( isAscii
                                                , isAlphaNum
                                                )
import           Data.List                      ( partition )

-- | Use url encoding to make a valid string
urlEncode :: ByteString -> ByteString
urlEncode "" = ""
urlEncode xs
  | (isAscii x && isAlphaNum x) || x `B.elem` "-_~." =  B.singleton x
  <> urlEncode (B.tail xs)
  | otherwise = escape (fromEnum x) <> urlEncode (B.tail xs)
 where
  x = B.head xs
  escape b =
    mconcat . fmap B.singleton $ ['%', showH (b `div` 16), showH (b `mod` 16)]
  showH i | i <= 9    = toEnum (fromEnum '0' + i)
          | otherwise = toEnum (fromEnum 'A' + (i - 10))

-- | Given a list of key value pairs generate a url encoded set
urlEncodeVars :: [(ByteString, ByteString)] -> ByteString
urlEncodeVars ((n, v) : t) =
  let (same, diff) = partition ((== n) . fst) t
  in  urlEncode n
        <> "="
        <> foldl' (\x y -> x <> "," <> urlEncode y) (urlEncode v) (map snd same)
        <> urlEncodeRest diff
 where
  urlEncodeRest []   = ""
  urlEncodeRest diff = "&" <> urlEncodeVars diff
urlEncodeVars [] = ""
