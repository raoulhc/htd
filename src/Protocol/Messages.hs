{-|
Module      : Protocol.Messages
Copyright   : (c) Raoul Hidalgo Charman, 2019
License     : BSD
Maintainer  : raoul.hidalgo.charman@gmail.com
Stability   : experimental

This contains low level methods for sending and receiving messages from the peer
wire protocol.
-}
module Protocol.Messages
  ( outHandshake
  , inHandshake
  , keepAlive
  , choke
  , unchoke
  , interested
  , uninterested
  , have
  , bitfield
  , request
  , piece
  , cancel
  , receive
  , pieceMsgs
  )
where

import           RIO                     hiding ( cancel
                                                , logInfo
                                                , logDebug
                                                , logError
                                                )
import qualified RIO.ByteString                as B
import qualified Data.ByteString.Char8         as BC
import           RIO.Partial                    ( toEnum )

import           Control.Exception              ( throw )
import           Control.Monad.Loops            ( iterateUntilM )
import           Control.Concurrent.STM.TSem
import qualified Data.Serialize                as S

import           Network.Socket.ByteString

import           Data.Peer
import           Data.Logging
import           Data.Bitfield
import           Data.Torrent
import           Data.MetaInfo
import           Data.Message
import           Data.Piece                     ( PieceLoc(..) )
import           System.Piece
import           Utils.BShow

-- | Construct handshake message for this torrent, send and
-- listen for response. Set peerID in the peer state
outHandshake :: (MonadReader PeerEnv m, MonadIO m) => m ()
outHandshake = do
  sendHandshake
  peerID <- recvHandshake
  logInfo $ "Succesful outgoing handshake with " <> displayShow peerID

-- | Listen for handshake message, check and then send handshake response
-- setting the peerID in the peer state
inHandshake :: (MonadReader PeerEnv m, MonadIO m) => m ()
inHandshake = do
  peerID <- recvHandshake
  sendHandshake
  bitfield
  logInfo $ "Succesful incoming handshake with " <> displayShow peerID

sendHandshake :: (MonadReader PeerEnv m, MonadIO m) => m ()
sendHandshake = do
  peerID   <- view $ torrentRL . peerIDL
  infoHash <- view $ torrentRL . metaInfoL . infoHashL
  let header   = "\19BitTorrent protocol"
      reserved = "\0\0\0\0\0\0\0\0"
      msg      = header <> reserved <> infoHash <> peerID
  s <- view socketL
  liftIO $ sendAll s msg

recvHandshake :: (MonadReader PeerEnv m, MonadIO m) => m ByteString
recvHandshake = do
  s        <- view socketL
  bs       <- liftIO $ recv s 68
  infoHash <- view $ torrentRL . metaInfoL . infoHashL
  when (B.length bs /= 68)
    $  logError
         (  "Failed handshake: wrong length handshake ("
         <> displayBytesUtf8 bs
         <> ")"
         )
    >> throw FailedHandshake
  let (rInfoHash, rPeerID) = B.splitAt 20 $ B.drop 28 bs
  when (rInfoHash /= infoHash)
    $  logError "Failed handshake: wrong infohash"
    >> throw FailedHandshake
  peerID <- view pidL
  atomically . writeTVar peerID $ Just rPeerID
  return rPeerID

-- | Generic message function
message
  :: (MonadReader PeerEnv m, MonadIO m)
  => Message -- ^ Message
  -> m ()
message m = do
  s <- view socketL
  let msg = S.encode m
  liftIO $ sendAll s msg

-- | Deserialise a message received from the socket waiting on socket until a
--   complete message is sent
receive :: (MonadReader PeerEnv m, MonadIO m) => m Message
receive = do
  -- get message length
  s      <- view socketL
  r1     <- liftIO $ recv s 4
  msgLen <- case (S.decode r1 :: Either String Word32) of
    Right x -> return x
    Left  e -> do
      logError
        .  displayBytesUtf8
        $  "Bad message ("
        <> bshow r1
        <> "): "
        <> BC.pack (show e)
      throw CorruptedResponse
  -- get rest of message
  r <- B.concat . reverse . snd <$> iterateUntilM
    (\(x, _) -> msgLen == x)
    (\(x, y) -> do
      let left = min 4096 $ msgLen - x
      r <- liftIO $ recv s (fromEnum left)
      -- when (r == "") $ throw EmptyResponse
      return (toEnum (B.length r) + x, r : y)
    )
    (0, [r1])
  case S.decode r of
    Left  _   -> throw CorruptedResponse
    Right msg -> return msg

keepAlive, choke, unchoke, interested, uninterested
  :: (MonadReader PeerEnv m, MonadIO m) => m ()
keepAlive = message KeepAlive
-- | Send choke message
choke = message Choke
-- | Send unchoke message
unchoke = message Unchoke
-- | Send interested message
interested = message Interested
-- | Sent uninterested message
uninterested = message Uninterested

-- | Send message stating a piece we have
have :: (MonadReader PeerEnv m, MonadIO m) => Word32 -> m ()
have = message . Have

-- | Send request message
request :: (MonadReader PeerEnv m, MonadIO m) => PieceLoc -> Word32 -> m ()
request pieceLoc len = do
  outReqs <- view outReqsL
  -- decrease semaphore and mark master piece as requested
  atomically $ waitTSem outReqs
  message $ Request pieceLoc len


-- | Send bitfield message
bitfield :: (MonadReader PeerEnv m, MonadIO m) => m ()
bitfield = do
  masterBitfield <- fmap (^. pieceBitFieldL) getEnv
  bs             <- tArrayToBitfield masterBitfield
  message $ Bitfield bs

-- | Send piece message
piece :: (MonadReader PeerEnv m, MonadIO m) => PieceLoc -> Word32 -> m ()
piece ix offset = do
  bs <- readBlockM ix offset
  message $ Piece ix bs

-- | Cancel a previously sent request
cancel :: (MonadReader PeerEnv m, MonadIO m) => PieceLoc -> Word32 -> m ()
cancel loc len = message $ Cancel loc len

blockSize :: Word32
blockSize = 2 ^ (15 :: Word32)

-- | Generate piece messages
pieceMsgs :: MetaInfo -> Word32 -> [(PieceLoc, Word32)]
pieceMsgs mi ix =
  let pieceLen = mi ^. infoDictL . pieceLengthL
      size     = mi ^. totalSizeL
      (chunkNum, lpsize) =
          if ix + 1 < size `div` pieceLen || size `mod` pieceLen == 0
            then (pieceLen `div` blockSize, blockSize)
            else
              ( (size `mod` pieceLen) `div` blockSize
              , (size `mod` pieceLen) `mod` blockSize
              )
  in  fmap (\(x, y) -> (PieceLoc ix x, y))
        $  zip [0, blockSize .. chunkNum * blockSize]
        $  replicate (fromEnum chunkNum - 1) blockSize
        <> [lpsize]
